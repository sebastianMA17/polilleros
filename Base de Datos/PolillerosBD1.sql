-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 12-04-2020 a las 15:07:57
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `institulac`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area`
--

DROP TABLE IF EXISTS `area`;
CREATE TABLE IF NOT EXISTS `area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1- activo',
  `fechar` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `area`
--

INSERT INTO `area` (`area_id`, `nombre`, `estado`, `fechar`) VALUES
(1, 'Area 12', '1', '2020-04-10 01:07:56'),
(2, 'Area 2', '1', '2020-04-10 01:31:21'),
(3, 'Area 3', '2', '2020-04-10 09:43:09'),
(4, 'area 4', '2', '2020-04-10 09:50:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_conocimiento`
--

DROP TABLE IF EXISTS `area_conocimiento`;
CREATE TABLE IF NOT EXISTS `area_conocimiento` (
  `area_conocimiento_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` char(45) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1-activo, 2-inactivo',
  `fechar` date DEFAULT NULL,
  PRIMARY KEY (`area_conocimiento_id`,`area_id`),
  KEY `fk_area_conocimiento_area1_idx` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `area_conocimiento`
--

INSERT INTO `area_conocimiento` (`area_conocimiento_id`, `area_id`, `nombre`, `estado`, `fechar`) VALUES
(1, 1, 'Area conocimiento 1', '1', '2020-04-10'),
(2, 2, 'Area conocimiento 22', '2', '2020-04-10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE IF NOT EXISTS `categoria` (
  `categoria_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `seccion` char(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1- Estudiante',
  `puntaje` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechar` date DEFAULT NULL,
  PRIMARY KEY (`categoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoria_id`, `nombre`, `seccion`, `puntaje`, `fechar`) VALUES
(1, 'NIVEL 10', '1', '10', '2020-04-07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `certificados_curriculum`
--

DROP TABLE IF EXISTS `certificados_curriculum`;
CREATE TABLE IF NOT EXISTS `certificados_curriculum` (
  `curriculum_certificado_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL COMMENT 'voy a insertar los tipos y subtpos para queme facilute las consultas',
  `subtipo_id` int(11) NOT NULL,
  `curriculum_id` int(11) NOT NULL,
  `puntaje_id` int(11) DEFAULT NULL,
  `certificado` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'Nombre del certificado',
  `archivo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'archivo o la ruta',
  `fechar` date DEFAULT NULL,
  PRIMARY KEY (`curriculum_certificado_id`,`tipo_id`,`subtipo_id`,`curriculum_id`),
  KEY `fk_certificados_curriculum_curriculum1_idx` (`curriculum_id`),
  KEY `fk_certificados_curriculum_puntaje_certificado1_idx` (`puntaje_id`),
  KEY `fk_certificados_curriculum_subtipo_certificado1_idx` (`subtipo_id`),
  KEY `fk_certificados_curriculum_tipos_certificado1_idx` (`tipo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
CREATE TABLE IF NOT EXISTS `ciudad` (
  `ciudad_id` int(11) NOT NULL AUTO_INCREMENT,
  `departamento_id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`ciudad_id`,`departamento_id`),
  KEY `fk_ciudad_departamento1_idx` (`departamento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ciudad`
--

INSERT INTO `ciudad` (`ciudad_id`, `departamento_id`, `nombre`) VALUES
(1, 1, 'Medellin'),
(2, 2, 'Bogotá');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentas`
--

DROP TABLE IF EXISTS `cuentas`;
CREATE TABLE IF NOT EXISTS `cuentas` (
  `cuenta_id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa` varchar(32) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechar` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`cuenta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `curriculum`
--

DROP TABLE IF EXISTS `curriculum`;
CREATE TABLE IF NOT EXISTS `curriculum` (
  `curriculum_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL,
  `departamento_id` int(11) NOT NULL,
  `ciudad_id` int(11) DEFAULT NULL,
  `nacionalidad` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `tipo_sexo` char(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1- Masculino',
  `estado_civil` char(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion_perfil` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `foto` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `total_puntaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`curriculum_id`,`usuario_id`,`departamento_id`),
  KEY `fk_curriculum_usuarios1_idx` (`usuario_id`),
  KEY `fk_curriculum_ciudad1_idx` (`departamento_id`),
  KEY `ciudad_id` (`ciudad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `curriculum`
--

INSERT INTO `curriculum` (`curriculum_id`, `usuario_id`, `departamento_id`, `ciudad_id`, `nacionalidad`, `fecha_nacimiento`, `tipo_sexo`, `estado_civil`, `descripcion_perfil`, `foto`, `total_puntaje`) VALUES
(1, 7, 1, 1, 'colombiana', '1997-08-13', '1', '1', 'dato actualizado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

DROP TABLE IF EXISTS `departamento`;
CREATE TABLE IF NOT EXISTS `departamento` (
  `departamento_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`departamento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`departamento_id`, `nombre`) VALUES
(1, 'Antioquia'),
(2, 'Cundinamarca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `disiplina`
--

DROP TABLE IF EXISTS `disiplina`;
CREATE TABLE IF NOT EXISTS `disiplina` (
  `disiplina_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechar` date DEFAULT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1-Activo, 2- Inactivo',
  PRIMARY KEY (`disiplina_id`,`area_id`),
  KEY `fk_disiplina_area_conocimiento1_idx` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `disiplina`
--

INSERT INTO `disiplina` (`disiplina_id`, `area_id`, `nombre`, `fechar`, `estado`) VALUES
(1, 1, 'Disiplina 1', '2020-04-10', '1'),
(2, 2, 'disiplina 1---', '2020-04-10', '2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos_semilleros`
--

DROP TABLE IF EXISTS `grupos_semilleros`;
CREATE TABLE IF NOT EXISTS `grupos_semilleros` (
  `grupo_id` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `semillero_id` int(11) NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT '1- aceptado',
  `mensaje_respuesta` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechar` date DEFAULT NULL,
  PRIMARY KEY (`grupo_id`,`usuario_id`,`semillero_id`),
  KEY `fk_usuarios_has_semilleros_semilleros1_idx` (`semillero_id`),
  KEY `fk_usuarios_has_semilleros_usuarios1_idx` (`usuario_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntaje`
--

DROP TABLE IF EXISTS `puntaje`;
CREATE TABLE IF NOT EXISTS `puntaje` (
  `puntaje_id` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`puntaje_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semilleros`
--

DROP TABLE IF EXISTS `semilleros`;
CREATE TABLE IF NOT EXISTS `semilleros` (
  `semillero_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_id` int(11) NOT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `area_conocimiento_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `disiplina_id` int(11) NOT NULL,
  `puntaje_id` int(11) DEFAULT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mensaje_respuesta` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `puntaje` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`semillero_id`,`area_id`,`area_conocimiento_id`,`usuario_id`,`disiplina_id`),
  KEY `fk_semilleros_area1_idx` (`area_id`),
  KEY `fk_semilleros_nivel1_idx` (`categoria_id`),
  KEY `fk_semilleros_area_conocimiento1_idx` (`area_conocimiento_id`),
  KEY `fk_semilleros_disiplina1_idx` (`disiplina_id`),
  KEY `fk_semilleros_usuarios1_idx` (`usuario_id`),
  KEY `fk_semilleros_puntaje1_idx` (`puntaje_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subtipo_certificado`
--

DROP TABLE IF EXISTS `subtipo_certificado`;
CREATE TABLE IF NOT EXISTS `subtipo_certificado` (
  `subtipo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL,
  `subtipo` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `otros` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`subtipo_id`,`tipo_id`),
  KEY `fk_subtipo_certificado_tipos_certificado1_idx` (`tipo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_certificado`
--

DROP TABLE IF EXISTS `tipos_certificado`;
CREATE TABLE IF NOT EXISTS `tipos_certificado` (
  `tipo_id` int(11) NOT NULL,
  `tipo` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `otros` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`tipo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) DEFAULT NULL,
  `rol_id` int(11) NOT NULL,
  `nombres` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellidos` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipoDoc` tinyint(4) DEFAULT NULL COMMENT '1- Cédula de ciudadani',
  `numDoc` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(40) COLLATE utf8_spanish_ci DEFAULT NULL,
  `psw` varchar(60) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` char(1) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '1' COMMENT '1-Activo, 2 Inactivo',
  `token_publico` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `fechar` date DEFAULT NULL,
  PRIMARY KEY (`usuario_id`,`rol_id`),
  KEY `fk_usuarios_roles1_idx` (`rol_id`),
  KEY `fk_usuarios_nivel1_idx` (`categoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `categoria_id`, `rol_id`, `nombres`, `apellidos`, `tipoDoc`, `numDoc`, `email`, `psw`, `estado`, `token_publico`, `fechar`) VALUES
(1, NULL, 1, 'Ivan Andres', 'Castillo Puentes', 1, '1080264480', 'ivan@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', NULL, NULL),
(2, NULL, 2, 'Juan Manuel', 'Vargas Liz', 1, '1212121212', 'juan@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', NULL, NULL),
(7, NULL, 4, 'Maria Alejandra', 'Vargas Liz', 1, '109152012', 'alejandra12vargasliz@gmail.com', 'a4ab582234ddd121e3b6857229e7d274', '1', '1586530846251-61320', '2020-04-09'),
(10, 1, 4, 'SEBASTIAN', 'MONTANA', 1, '123456', 'SLNDLFANS@LDJLDSJA.COM', '123456', '1', NULL, '2020-04-06'),
(13, NULL, 3, 'Maria Alejandra', 'Vargas Liz', 2, '109152012', 'tutor1li@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1', NULL, NULL),
(14, NULL, 3, 'Juan Manuel', 'vargas Liz', 1, '1092019837', 'juanmanuel@gmail.com', NULL, '2', NULL, NULL),
(15, NULL, 3, 'Ivan', 'Castillo', 2, '121212', 'andrescastilloweb@gmail.com', NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usu_rol`
--

DROP TABLE IF EXISTS `usu_rol`;
CREATE TABLE IF NOT EXISTS `usu_rol` (
  `rol_id` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`rol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usu_rol`
--

INSERT INTO `usu_rol` (`rol_id`, `rol`) VALUES
(1, 'SuperAdmin'),
(2, 'Administrador'),
(3, 'Tutor'),
(4, 'Estudiante');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `area_conocimiento`
--
ALTER TABLE `area_conocimiento`
  ADD CONSTRAINT `area_conocimiento_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`);

--
-- Filtros para la tabla `certificados_curriculum`
--
ALTER TABLE `certificados_curriculum`
  ADD CONSTRAINT `fk_certificados_curriculum_curriculum1` FOREIGN KEY (`curriculum_id`) REFERENCES `curriculum` (`curriculum_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_certificados_curriculum_puntaje_certificado1` FOREIGN KEY (`puntaje_id`) REFERENCES `puntaje` (`puntaje_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_certificados_curriculum_subtipo_certificado1` FOREIGN KEY (`subtipo_id`) REFERENCES `subtipo_certificado` (`tipo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_certificados_curriculum_tipos_certificado1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos_certificado` (`tipo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ciudad`
--
ALTER TABLE `ciudad`
  ADD CONSTRAINT `fk_ciudad_departamento1` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`departamento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `curriculum`
--
ALTER TABLE `curriculum`
  ADD CONSTRAINT `curriculum_ibfk_1` FOREIGN KEY (`departamento_id`) REFERENCES `departamento` (`departamento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `curriculum_ibfk_2` FOREIGN KEY (`ciudad_id`) REFERENCES `ciudad` (`ciudad_id`),
  ADD CONSTRAINT `fk_curriculum_usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `disiplina`
--
ALTER TABLE `disiplina`
  ADD CONSTRAINT `disiplina_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`);

--
-- Filtros para la tabla `grupos_semilleros`
--
ALTER TABLE `grupos_semilleros`
  ADD CONSTRAINT `fk_usuarios_has_semilleros_semilleros1` FOREIGN KEY (`semillero_id`) REFERENCES `semilleros` (`semillero_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_semilleros_usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `semilleros`
--
ALTER TABLE `semilleros`
  ADD CONSTRAINT `fk_semilleros_area_conocimiento1` FOREIGN KEY (`area_conocimiento_id`) REFERENCES `area_conocimiento` (`area_conocimiento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_semilleros_disiplina1` FOREIGN KEY (`disiplina_id`) REFERENCES `disiplina` (`disiplina_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_semilleros_nivel1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`categoria_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_semilleros_puntaje1` FOREIGN KEY (`puntaje_id`) REFERENCES `puntaje` (`puntaje_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_semilleros_usuarios1` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`usuario_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `semilleros_ibfk_1` FOREIGN KEY (`semillero_id`) REFERENCES `area` (`area_id`);

--
-- Filtros para la tabla `subtipo_certificado`
--
ALTER TABLE `subtipo_certificado`
  ADD CONSTRAINT `fk_subtipo_certificado_tipos_certificado1` FOREIGN KEY (`tipo_id`) REFERENCES `tipos_certificado` (`tipo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_nivel1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`categoria_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_roles1` FOREIGN KEY (`rol_id`) REFERENCES `usu_rol` (`rol_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
